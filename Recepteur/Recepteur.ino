#define RELAY_OUTPUT_PIN 2

long boundRate = 9600;

const byte numChars = 40;//allows for 2 digit month/day
char receivedChars[numChars];// a char array to store the received data
char endMarker = '\n';

//control flags
boolean newData = false;

int inc_byte;



void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);  // Moniteur serie
  Serial1.begin(9600);  // Port serie pour la communication Xbee
  pinMode(RELAY_OUTPUT_PIN, OUTPUT); // Output du relay pin 2
 
}

void loop() {

  // put your main code here, to run repeatedly:
  
  if (Serial.available() > 0){
    
      inc_byte = Serial.read();

      if(inc_byte==49){
        digitalWrite(RELAY_OUTPUT_PIN, HIGH);
       } else if(inc_byte==48) {
          digitalWrite(RELAY_OUTPUT_PIN, LOW);
        } 
        
  } // fin if.
  
  
  receveAvecTab();
  afficherReceve();
   
}


void receveAvecTab() {
  static byte ndx = 0;
  char endMarker = '\n';
  char rc;

  if (Serial1.available() > 0) {
    rc = Serial1.read();

    if (rc != endMarker) {
      receivedChars[ndx] = rc;
      ndx++;
      if (ndx >= numChars) {
        ndx = numChars - 1;
      }
    }
    else {
      receivedChars[ndx] = '\0'; // null terminate the string
      ndx = 0;
      newData = true;
    }
  }
}

void afficherReceve() {
  if (newData == true) {
    Serial.println(receivedChars);
    newData = false;
  }
}
