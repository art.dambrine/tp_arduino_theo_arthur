// inclusion lib lcd et wire
#include <math.h>
#include <Wire.h>
#include <Grove_LED_Bar.h>
#include "rgb_lcd.h"

// defines du potar

#define ROTARY_ANGLE_SENSOR A1
#define ADC_REF 5 //reference voltage of ADC is 5v.If the Vcc switch on the seeeduino
                    //board switches to 3V3, the ADC_REF should be 3.3
#define GROVE_VCC 5 //VCC of the grove interface is normally 5v
#define FULL_ANGLE 50 //full value of the rotary angle is 300 degrees

// constantes capteur de température

const int B = 4275;               // B value of the thermistor
const int R0 = 100000;            // R0 = 100k
const int pinTempSensor = A0;     // Grove - Temperature Sensor connect to A0

// constantes pour le lcd

rgb_lcd lcd;
const int colorR = 0; // niveau sur 1 octet 0-255
const int colorG = 0;
const int colorB = 0;

// constantes pour le bouton
const int buttonPin = 5;     // the number of the pushbutton pin
const int ledPin =  13;
int defautMode = 0;
int flag = 0;   

void setup()
{
  Serial.begin(9600);//Moniteur
  Serial1.begin(9600);//Xbee
  
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  
  lcd.setRGB(colorR, colorG, colorB);

  pinMode(ROTARY_ANGLE_SENSOR, INPUT);  // setup du potar sur INPUT pin ROTARY_ANGLE_SENSOR= A1
  pinMode(6, OUTPUT);//buzzer

  pinMode(ledPin, OUTPUT);
  pinMode(buttonPin, INPUT);

  pinMode(A2, INPUT);
  pinMode(A3, INPUT);
}


void loop()
{
  
  int etatBouton = digitalRead(buttonPin); 
  float consigne = potar();
  float temperature = temperatureSensor();
  float luminosite = lightSensor();
  float son  = soundSensor();
  afficheLCD(temperature,consigne); // gestion de l'affichage lcd
  EnvoiXbee(temperature,consigne,luminosite, son);
  
  //gestion du bouton
  if (etatBouton == HIGH) {
        if(defautMode == 1){
          defautMode = 0;
          digitalWrite(ledPin, LOW);
        }
        else{
          defautMode = 1;
          digitalWrite(ledPin, HIGH);
        }
        
  }
  
  //gestion du buzzer
  if(consigne < 10 && defautMode == 0 && flag == 0){
    digitalWrite(6, HIGH);
    delay(100);
    digitalWrite(6, LOW);
    delay(100);
    digitalWrite(6, HIGH);
    delay(100);
    digitalWrite(6, LOW);
    flag = 1;}
   else if (consigne >=10) {
    digitalWrite(6, LOW);
    flag = 0;
   }
   delay(500);
}


//  ============================================================
//  =================   NOS SUPER FONCTIONS ====================
//  ============================================================

void EnvoiXbee(float temperature, float consigne, float luminosite,float son)
{
 
 // Temperature et consigne au recepteur Xbee
 String cons = "c:" ; String temp = ",t:"; String lum = ",l:"; String so = ",s:";
 String texte = cons + consigne + temp + temperature + lum + luminosite + so + son; 
 Serial1.println(texte);
 Serial.println(texte);
 
}

void afficheLCD(float temperature, float consigne){

  //Initialisation des chaines de caractere a afficher
  String stringOne = "Temp = ", stringTwo = "C", stringThree = "Cons = ";
  String texteTemp = stringOne  + temperature + (char)223 + stringTwo;
  String texteCons = stringThree  + consigne + (char)223 + stringTwo;
  if(consigne < 10){
    String stringZero = "0";
    texteCons = stringThree  + stringZero + consigne + (char)223 + stringTwo;
  }
  
  //Placement sur la premiere ligne du LCD
  lcd.setCursor(0, 0);
  //Affichage de la température sur la premiere ligne
  lcd.print(texteTemp);
  //Placement sur la seconde ligne du LCD
  lcd.setCursor(0, 1);
  //Affichage de la consigne sur la premiere ligne
  lcd.print(texteCons); 

  // Affichage d'un fond led coloré en fonction du respect de la consigne :
  if(temperature > consigne +0.5 ){
    // joli rouge, mais attention il fait chaud quand même
    lcd.setRGB(255, 102, 0); 
  }
  else if(temperature < consigne - 0.5){
    // joli bleu, mais attention il fait froid quand même
    lcd.setRGB(51,204,255);
  }
  else {
    // green, on est bien
    lcd.setRGB(51,204,51);
  } 

  
}
float potar()
{   
    float voltage;
    int sensor_value = analogRead(ROTARY_ANGLE_SENSOR);
    voltage = (float)sensor_value*ADC_REF/1023;
    float degrees = (voltage*FULL_ANGLE)/GROVE_VCC;
    
    float consigne = mapfloat(degrees, 0, FULL_ANGLE, 30, 5); // mettre un pas de 0,5 soit /50 5->30C
    return consigne;
}
float temperatureSensor(){
  // lecture de la valeur envoyée par le capt de temp.
  int a = analogRead(pinTempSensor);    
  float R = 1023.0 / a - 1.0;
  R = R0 * R;
  // convert to temperature en passant par la datasheet cf.cours capteurs
  return 1.0 / (log(R / R0) / B + 1 / 298.15) - 273.15; 
}
float lightSensor(){
  int value = analogRead(A2);
  value = map(value, 0, 800, 0, 10);
  return value;
}
float soundSensor(){
  int value = analogRead(A3);
  //value = map(value, 0, 800, 0, 10);
  return value;
}
float mapfloat(long x, long in_min, long in_max, long out_min, long out_max) // notre fonction map qui renvoie un float au lieu d'un long.
{
 return (float)(x - in_min) * (out_max - out_min) / (float)(in_max - in_min) + out_min;
}
