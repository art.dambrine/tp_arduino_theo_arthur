// Relay Control
#define RELAY_OUTPUT_PIN 2 // PIN UART S2RIE

void setup()
{
  pinMode(RELAY_OUTPUT_PIN, OUTPUT); // Output du relay pin 2
}

void loop()
{
  
    digitalWrite(RELAY_OUTPUT_PIN, HIGH);
    delay(2000);

    digitalWrite(RELAY_OUTPUT_PIN, LOW);
    delay(2000);
}
